#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Button.H>
#include "ui.hpp"
#include "tray.hpp"
#include "modules/kbd.hpp"
#include "modules/av.hpp"
#include "modules/power.hpp"

using namespace SWITCHBOARD;
UI::UI(int width, int height) {
	window = new Fl_Window(width, height, "Switchboard");
	this->width = width; this->height = height;
	this->tray_btn_height = height * 0.1;
	theme_up = FL_GTK_UP_BOX;
	theme_dn = FL_GTK_DOWN_BOX;
	mode = MODE::KBD;
	kbd = new Keyboard(this);
	av = new AudioVideo(this);
	pwr = new Power(this);
	min = new Minimize(this);
	// windows, notifications, radios
	tray = new Tray(this); // init last as it depends on others->traybtn
	((Keyboard*)kbd)->show();
}

