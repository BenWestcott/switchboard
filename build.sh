#!/bin/sh
files="
ui.cpp
tray.cpp
modules/av.cpp
modules/power.cpp
modules/kbd.cpp
modules/kbd_key.cpp
modules/kbd_sim.cpp
modules/minimize.cpp
main.cpp
"
links="$(fltk-config --ldflags --cxxflags) -lXtst -lX11"
warns="-Wall -Wextra --pedantic"

g++ -O0 -g -fstack-protector $files -o switchboard $links $warns

