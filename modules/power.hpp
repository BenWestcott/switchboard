#ifndef SWBD_MODULE_POWER_H
#define SWBD_MODULE_POWER_H
#include "../ui.hpp"
#include <FL/Fl_Button.H>

namespace SWITCHBOARD {
using namespace SWITCHBOARD;
class Power {
	public:
	Power(UI* ui);
	void show(void);
	void hide(void);
	Fl_Button* traybtn;

	private:
	static void callbackTray(Fl_Widget* w, void* power);
	static void callbackLock(Fl_Widget* w);
	static void callbackScreenOff(Fl_Widget* w);
	static void callbackSuspend(Fl_Widget* w);
	static void callbackLogout(Fl_Widget* w);
	static void callbackHalt(Fl_Widget* w);
	static void callbackReboot(Fl_Widget* w);
	UI* ui;
	Fl_Button *lock, *screenOff, *suspend, *logout, *halt, *reboot;
};
} // namespace
#endif

