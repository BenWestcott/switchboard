#include "power.hpp"
#include "../tray.hpp"
#include <FL/Fl_Button.H>
#include <unistd.h>

using namespace SWITCHBOARD;

void Power::callbackLock(Fl_Widget* w) {
	(void)w;
	if (fork() == 0)
		exit(execlp("sxmo_lock.sh", "sxmo_lock.sh", NULL) != 0);
}
void Power::callbackScreenOff(Fl_Widget* w) {
	(void)w;
	if (fork() == 0)
		execlp("sxmo_lock.sh", "sxmo_lock.sh", "--screen-off", NULL);
}
void Power::callbackSuspend(Fl_Widget* w) {
	(void)w;
	if (fork() == 0)
		execlp("sxmo_lock.sh", "sxmo_lock.sh", "--suspend", NULL);
}
void Power::callbackLogout(Fl_Widget* w) {
	(void)w;
	execlp("pkill", "pkill", "-9", "dwm", NULL);
}
void Power::callbackHalt(Fl_Widget* w) {
	(void)w;
	execlp("lxterminal", "lxterminal", "-e", "sudo halt", NULL);
}
void Power::callbackReboot(Fl_Widget* w) {
	(void)w;
	execlp("lxterminal", "lxterminal", "-e", "sudo reboot", NULL);
}


void Power::show() {
	lock->show();
	screenOff->show();
	suspend->show();
	logout->show();
	halt->show();
	reboot->show();
}

void Power::hide() {
	lock->hide();
	screenOff->hide();
	suspend->hide();
	logout->hide();
	halt->hide();
	reboot->hide();
}

void Power::callbackTray(Fl_Widget* w, void* power) {
	(void)w;
	Power* pwr = (Power*)power;
	((Tray*)pwr->ui->tray)->hide();
	pwr->show();
	pwr->ui->mode = UI::MODE::POWER;
}

Power::Power(UI* ui) {
	this->ui = ui;
	int width = ui->width / 3;
	int height = (ui->height - ui->tray_btn_height) / 2;
	lock = new Fl_Button(0, 0, width, height, "Lock");
	lock->hide();
	lock->box(ui->theme_up);
	lock->callback(Power::callbackLock);
	screenOff = new Fl_Button(width, 0, width, height, "Screen off");
	screenOff->hide();
	screenOff->box(ui->theme_up);
	screenOff->callback(Power::callbackScreenOff);
	suspend = new Fl_Button(width*2, 0, width, height, "Suspend");
	suspend->hide();
	suspend->box(ui->theme_up);
	suspend->callback(Power::callbackSuspend);
	logout = new Fl_Button(0, height, width, height, "Logout");
	logout->hide();
	logout->box(ui->theme_up);
	logout->callback(Power::callbackLogout);
	halt = new Fl_Button(width, height, width, height, "Shutdown");
	halt->hide();
	halt->box(ui->theme_up);
	halt->callback(Power::callbackHalt);
	reboot = new Fl_Button(width*2, height, width, height, "Reboot");
	reboot->hide();
	reboot->box(ui->theme_up);
	reboot->callback(Power::callbackReboot);

	traybtn = new Fl_Button(0, 0, 0, 0, "Power");
	traybtn->hide();
	traybtn->box(ui->theme_up);
	traybtn->callback(Power::callbackTray, this);
}

