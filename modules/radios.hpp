#ifndef SWBD_MODULE_RADIOS_H
#define SWBD_MODULE_RADIOS_H
#include "../ui.hpp"
#include <FL/Fl_Button.H>
// stubs
Fl_Button* swbd_module_radios_init(swbd_win* w) {
	Fl_Button* b = new Fl_Button(0, 0, 0, 0, "radios stub");
	b->hide();
	b->box(w->theme_up);
	return b;
}
void swbd_module_radios_show(swbd_win* w) {
	(void)w;
}
void swbd_module_radios_hide(swbd_win* w) {
	(void)w;
}

#endif

