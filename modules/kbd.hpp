#ifndef SWBD_MODULE_KBD_H
#define SWBD_MODULE_KBD_H
#include "../ui.hpp"
#include <FL/Fl_Button.H>

namespace SWITCHBOARD {
using namespace SWITCHBOARD;
class Key {
	public:
	Key(Fl_Button* b, int x, int y, int w, int h);
	void hide(void);
	void show(void);
	Fl_Button* button;
	unsigned short x, y, w, h;
};
class KeySim {
	public:
	KeySim(void);
	~KeySim(void);
	static void callback(Fl_Widget* w, void* simDat);
	private:
	void* simDat;
};
class Keyboard {
	public:
	Keyboard(UI* ui);
	void show(void);
	void hide(void);
	Fl_Button* traybtn;

	private:
	static void callbackTray(Fl_Widget* w, void* keyboard);
	Key* add(Fl_Button* b, const char* title, int col, int width_cols,
		int total_cols, int y, int height);
	void initBtns(void);
	void initKeys(void);

	UI* ui;
	KeySim* sim;
	unsigned short mode;
	Fl_Button *Shift, *Fn, *Ctrl, *Alt, *Caps;
	Fl_Button *_1, *_2, *_3, *_4, *_5, *_6, *_7, *_8, *_9, *_0;
	Fl_Button *kp1, *kp2, *kp3, *kp4, *kp5, *kp6, *kp7, *kp8, *kp9, *kp0;
	Fl_Button *F1, *F2, *F3, *F4, *F5, *F6, *F7, *F8, *F9, *F10, *F11, *F12;
	Fl_Button *A, *B, *C, *D, *E, *F, *G, *H, *I, *J, *K, *L, *M,
		*N, *O, *P, *Q, *R, *S, *T, *U, *V, *W, *X, *Y, *Z;
	Fl_Button *comma, *period, *slash, *backslash, *dash, *equals, *quote, *grave,
		*semicolon, *open_bracket, *close_bracket, *Home, *PgUp, *PgDn, *End,
		*Esc, *Enter, *backspace, *Del, *space, *tab, *left, *down, *up, *right,
		*PrtSc, *Menu;
	Key* row_0_0[12];
	Key* row_0_1[11];
	Key* row_0_2[11];
	Key* row_0_3[11];
	Key* row_0_4[9];
	Key* row_1_0[12];
	Key* row_1_1[11];
	Key* row_1_2[11];
	Key* row_1_3[11];
	Key* row_1_4[9];
};
} // namespace
#endif

