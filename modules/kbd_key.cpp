#include "kbd.hpp"

using namespace SWITCHBOARD;

Key::Key(Fl_Button* b, int x, int y, int w, int h) {
	this->button = b;
	this->x = x; this->y = y;
	this->w = w; this->h = h;
}

void Key::hide(void) {
	button->hide();
}

void Key::show(void) {
	button->resize(x, y, w, h);
	button->show();
}

