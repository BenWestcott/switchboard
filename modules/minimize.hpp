#ifndef SWBD_MODULE_MINIMIZE_H
#define SWBD_MODULE_MINIMIZE_H
#include "../ui.hpp"
#include <FL/Fl_Button.H>

namespace SWITCHBOARD{
using namespace SWITCHBOARD;
class Minimize {
	public:
	Minimize(UI* ui);
	void show(void);
	void hide(void);
	Fl_Button* traybtn;
	private:
	UI* ui;
	static void callback(Fl_Widget* w, void* minimize);
	static void callbackDummy(Fl_Widget* w, void* v);
	friend class Tray;
};
} // namespace
#endif

