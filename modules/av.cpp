#include "av.hpp"
#include "../tray.hpp"
#include <stdio.h>
#include <unistd.h>

// Set an ALSA control (C API is undocumented)
#define AMSET(...) \
	if (fork() == 0) \
		exit(execlp("amixer", "amixer", "-q", "set", __VA_ARGS__, NULL) != 0);

using namespace SWITCHBOARD;

AudioVideo::AudioVideo(UI* ui) {
	this->ui = ui;
	// 10% padding on each side
	int x = ui->width * 0.1;
	int y = ui->height * 0.1;
	int width = ui->width - ui->width*0.2;
	int height = ui->height*0.5 - ui->height*0.2;
	// Set up volume slider
	volume = new Fl_Hor_Slider(x, y, width*0.7, height, "Volume");
	volume->hide();
	volume->callback(AudioVideo::callbackVolume, this);
	volume->slider(ui->theme_up);
	volume->box(ui->theme_dn);
	volume->bounds(0, 100);
	volume->step(1);
	// Set up audio device dropdown
	strcpy(&audioOut[0], "None");
	audioDevice = new Fl_Choice(x+width*0.7, y, width*0.3, height, "");
	audioDevice->hide();
	audioDevice->box(ui->theme_up);
	audioDevice->down_box(ui->theme_dn);
	audioDevice->add("None",       0, AudioVideo::callbackDevice, this);
	audioDevice->add("Earpiece",   0, AudioVideo::callbackDevice, this);
	audioDevice->add("Speaker",    0, AudioVideo::callbackDevice, this);
	audioDevice->add("Headphones", 0, AudioVideo::callbackDevice, this);
	audioDevice->value(3); // default to headphones
	// Set up brightness slider
	y += height + (ui->height*0.1);
	brightness = new Fl_Hor_Slider(x, y, width, height, "Brightness");
	brightness->hide();
	brightness->callback(AudioVideo::callbackBrightness);
	brightness->slider(ui->theme_up);
	brightness->box(ui->theme_dn);
	brightness->bounds(2, 1000);
	brightness->step(1);

	// Set up tray button
	traybtn = new Fl_Button(0, 0, 0, 0, "Audio/Video");
	traybtn->hide();
	traybtn->box(ui->theme_up);
	traybtn->callback(AudioVideo::callbackTray, this);
}

int AudioVideo::getVolume(char* audioOut) {
	if (strcmp(audioOut, "None") == 0)
		return 0;
	char cmd[128];
	snprintf(&cmd[0], 128,
		"amixer get \"%s\" | grep -Eom1 \"\\[[0-9]*%%\\]\"", audioOut);
	FILE* out = popen(&cmd[0], "r");
	int vol;
	int scan = fscanf(out, "[%d%%]", &vol);
	pclose(out);
	if (scan != 1) {
		printf("Error getting volume: %i\n", scan);
		return 0;
	}
	return vol;
}

int AudioVideo::getBacklight() {
	const char p[42] = "/sys/class/backlight/backlight/brightness";
	const char* path = &p[0];
	FILE* f = fopen(path, "r");
	if (!f) {
		printf("Error opening file for reading: \"%s\"\n", path);
		return 0;
	}
	int value;
	int scan = fscanf(f, "%d", &value);
	if (scan != 1) {
		printf("Error scanning for integer, got %i items: \"%s\"\n", scan, path);
		fclose(f);
		return 0;
	}
	return value;
}

void AudioVideo::callbackVolume(Fl_Widget* w, void* audioVideo) {
	Fl_Hor_Slider* slider = (Fl_Hor_Slider*)w;
	AudioVideo* av = (AudioVideo*)audioVideo;
	int vol = slider->value();
	char volarg[8];
	snprintf(&volarg[0], 8, "%i%%", vol);
	AMSET(av->audioOut, &volarg[0])
}

void AudioVideo::callbackBrightness(Fl_Widget* w) {
	Fl_Hor_Slider* slider = (Fl_Hor_Slider*)w;
	int bright = slider->value();
	const char p[42] = "/sys/class/backlight/backlight/brightness";
	const char* path = &p[0];
	FILE* f = fopen(path, "w");
	if (!f) {
		printf("Error opening file for writing: \"%s\"\n", path);
		return;
	}
	int written = fprintf(f, "%d", bright);
	fclose(f);
	if (written <= 0)
		printf("Error writing, code %i: \"%s\"\n", written, path);
}

void AudioVideo::callbackDevice(Fl_Widget* w, void* audioVideo) {
	Fl_Choice* choice = (Fl_Choice*)w;
	AudioVideo* av = (AudioVideo*)audioVideo;
	switch(choice->value()) {
		case 0:
			AMSET("Earpiece", "mute")
			AMSET("Line Out", "mute")
			AMSET("Headphone", "mute")
			strcpy(av->audioOut, "None");
			break;
		case 1:
			AMSET("Line Out", "mute")
			AMSET("Headphone", "mute")
			strcpy(av->audioOut, "Earpiece");
			break;
		case 2:
			AMSET("Earpiece", "mute")
			AMSET("Headphone", "mute")
			strcpy(av->audioOut, "Line Out");
			break;
		case 3:
			AMSET("Earpiece", "mute")
			AMSET("Line Out", "mute")
			strcpy(av->audioOut, "Headphone"); 
			break;
		default:
			printf("Unexpected choice: %i\n", choice->value());
	}
	if (av->audioOut[0] != 'N') {
		AMSET(av->audioOut, "unmute")
		av->volume->value(getVolume(av->audioOut));
	}
	(void)system("pgrep -f \"$(command -v sxmo_statusbar.sh)\" | xargs kill -USR1 &");
}
#undef AMSET

void AudioVideo::callbackTray(Fl_Widget* btn, void* audioVideo) {
	(void)btn;
	AudioVideo* av = (AudioVideo*)audioVideo;
	((Tray*)av->ui->tray)->hide();
	av->show();
	av->ui->mode = UI::MODE::AV;
}

void AudioVideo::hide(void) {
	volume->hide();
	brightness->hide();
	audioDevice->hide();
}

void AudioVideo::show(void) {
	volume->value(getVolume(audioOut));
	volume->show();
	brightness->value(AudioVideo::getBacklight());
	brightness->show();
	audioDevice->show();
}

