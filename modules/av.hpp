#ifndef SWBD_MODULE_AV_H
#define SWBD_MODULE_AV_H
#include "../ui.hpp"
#include <FL/Fl_Button.H>
#include <FL/Fl_Hor_Slider.H>
#include <FL/Fl_Choice.H>

namespace SWITCHBOARD {
using namespace SWITCHBOARD;
class AudioVideo {
	public:
	AudioVideo(UI* ui);
	void show(void);
	void hide(void);
	Fl_Button* traybtn;

	private:
	static int getVolume(char* audioOut);
	static int getBacklight(void);
	static void callbackTray(Fl_Widget* w, void* audioVideo);
	static void callbackVolume(Fl_Widget* w, void* audioVideo);
	static void callbackBrightness(Fl_Widget* w);
	static void callbackDevice(Fl_Widget* w, void* audioVideo);
	UI* ui;
	Fl_Hor_Slider *volume, *brightness;
	Fl_Choice* audioDevice;
	char audioOut[16];
};
} // namespace
#endif

