#ifndef SWBD_MODULE_NOTIFS_H
#define SWBD_MODULE_NOTIFS_H
#include "../ui.hpp"
#include <FL/Fl_Button.H>
// stubs
Fl_Button* swbd_module_notifs_init(swbd_win* w) {
	Fl_Button* b = new Fl_Button(0, 0, 0, 0, "notifs stub");
	b->hide();
	b->box(w->theme_up);
	return b;
}
void swbd_module_notifs_show(swbd_win* w) {
	(void)w;
}
void swbd_module_notifs_hide(swbd_win* w) {
	(void)w;
}

#endif

