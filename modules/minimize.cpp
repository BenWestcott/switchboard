#include "minimize.hpp"
#include "../tray.hpp"
#include "kbd.hpp"

using namespace SWITCHBOARD;

Minimize::Minimize(UI* ui) {
	this->ui = ui;
	traybtn = new Fl_Button(0, 0, 0, 0, "<");
	traybtn->callback(Minimize::callback, this);
	traybtn->when(FL_WHEN_CHANGED);
	traybtn->hide();
	traybtn->box(ui->theme_up);
}

void Minimize::show(void) {
	traybtn->show();
}
void Minimize::hide(void) {
	traybtn->hide();
}

void Minimize::callback(Fl_Widget* widget, void* minimize) {
	if (!((Fl_Button*)widget)->value())
		return;
	Minimize* mm = (Minimize*)minimize;
	UI* ui = mm->ui;
	Tray* tr = (Tray*)ui->tray;
	Keyboard* kb = (Keyboard*)ui->kbd;

	if (ui->window->h() > ui->tray_btn_height*1.2) { // not minimized
		tr->toggle->callback(Minimize::callbackDummy);
		tr->hide();
		kb->hide();
		tr->minimize->position(0, 0);
		tr->toggle->position(ui->width*0.2, 0);
		//tr->windows->traybtn->position(ui->width*0.8, 0);
		ui->window->size(ui->width, ui->tray_btn_height);
		tr->minimize->label(">");
	} else { // minimized
		ui->window->size(ui->width, ui->height);
		tr->toggle->position(ui->width * 0.2, ui->height - ui->tray_btn_height);
		tr->minimize->position(0, ui->height - ui->tray_btn_height);
		//tr->windows->position(ui->width * 0.8,
		//		ui->height - ui->tray_btn_height);
		ui->mode = UI::MODE::KBD;
		kb->show();
		tr->toggle->callback(Tray::callback, tr);
		tr->minimize->label("<");
	}
}

void Minimize::callbackDummy(Fl_Widget* w, void* v) {
	(void)w; (void)v;
}

