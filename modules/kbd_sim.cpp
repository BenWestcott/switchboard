#ifndef SWBD_MODULE_KBDSIM_C
#define SWBD_MODULE_KBDSIM_C
#include "kbd.hpp"
#include "../ui.hpp"
#include <FL/Fl_Button.H>
#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <X11/extensions/XTest.h>

using namespace SWITCHBOARD;

typedef struct {
	Display* display;
	unsigned int keymask;
} swbd_module_kbd_simdata;

KeySim::KeySim(void) {
	simDat = malloc(sizeof(swbd_module_kbd_simdata));
	swbd_module_kbd_simdata* d = (swbd_module_kbd_simdata*)simDat;
	d->keymask = 0;
	d->display = XOpenDisplay(NULL);
	if (!d->display) {
		printf("Unknown error attempting to open the X display.");
		exit(1);
	}
}
KeySim::~KeySim(void) {
	free(simDat);
}

/** Helper function to convert a keysym to a keycode
 *  Modifier status is stored persistently
 *  @param f Pointer to the persistent data store
 *  @param keysym X11 key identifier
 *  @return Generated keycode for use with XTestFakeKeyEvent
 */
static unsigned int get_keycode(swbd_module_kbd_simdata* d,
		unsigned int keysym) {
	unsigned int keycode;
	switch (keysym) {
		case XK_Shift_L:
		case XK_Control_L:
		case XK_Meta_L:
			// do something to d->keymask
		default:
			keycode = XKeysymToKeycode(d->display,
				keysym | d->keymask);
	}
	return keycode;
}

void KeySim::callback(Fl_Widget* widget, void* simDat) {
	Fl_Button* b = (Fl_Button*)widget;
	swbd_module_kbd_simdata* d = (swbd_module_kbd_simdata*)simDat;
	unsigned int keycode = get_keycode(d, XK_A);
	if (b->value())
		XTestFakeKeyEvent(d->display, keycode, 1, 0);
	else
		XTestFakeKeyEvent(d->display, keycode, 0, 0);
}

#endif

