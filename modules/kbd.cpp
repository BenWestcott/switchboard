#include "kbd.hpp"
#include "../tray.hpp"
#include "../ui.hpp"
#include <FL/Fl.H>
#include <FL/Fl_Button.H>
#include <X11/keysym.h>

using namespace SWITCHBOARD;

/** Init Key: allocates a single button, for kbd_initbtns
 *  TODO: make this less hacky (no idea how though)
 */
#define SWBD_IK(k) \
	k = new Fl_Button(0, 0, 10, 10, #k); \
	k->hide(); k->when(FL_WHEN_CHANGED); \
	k->callback(KeySim::callback, sim);

/** Allocates all the FLTK button objects
 */
void Keyboard::initBtns(void) {
	SWBD_IK(Shift) SWBD_IK(Fn) SWBD_IK(Ctrl) SWBD_IK(Alt) SWBD_IK(Caps)
	SWBD_IK(_1) SWBD_IK(_2) SWBD_IK(_3) SWBD_IK(_4) SWBD_IK(_5)
	SWBD_IK(_6) SWBD_IK(_7) SWBD_IK(_8) SWBD_IK(_9) SWBD_IK(_0)
	SWBD_IK(kp1) SWBD_IK(kp2) SWBD_IK(kp3) SWBD_IK(kp4) SWBD_IK(kp5)
	SWBD_IK(kp6) SWBD_IK(kp7) SWBD_IK(kp8) SWBD_IK(kp9) SWBD_IK(kp0)
	SWBD_IK(F1) SWBD_IK(F2) SWBD_IK(F3) SWBD_IK(F4) SWBD_IK(F5) SWBD_IK(F6)
	SWBD_IK(F7) SWBD_IK(F8) SWBD_IK(F9) SWBD_IK(F10) SWBD_IK(F11) SWBD_IK(F12)
	SWBD_IK(A) SWBD_IK(B) SWBD_IK(C) SWBD_IK(D) SWBD_IK(E) SWBD_IK(F) SWBD_IK(G)
	SWBD_IK(H) SWBD_IK(I) SWBD_IK(J) SWBD_IK(K) SWBD_IK(L) SWBD_IK(M) SWBD_IK(N)
	SWBD_IK(O) SWBD_IK(P) SWBD_IK(Q) SWBD_IK(R) SWBD_IK(S) SWBD_IK(T) SWBD_IK(U)
	SWBD_IK(V) SWBD_IK(W) SWBD_IK(X) SWBD_IK(Y) SWBD_IK(Z)
	SWBD_IK(comma) SWBD_IK(period) SWBD_IK(slash) SWBD_IK(backslash) SWBD_IK(dash)
	SWBD_IK(equals) SWBD_IK(quote) SWBD_IK(grave)
	SWBD_IK(semicolon) SWBD_IK(open_bracket) SWBD_IK(close_bracket) SWBD_IK(Home)
	SWBD_IK(PgUp) SWBD_IK(PgDn) SWBD_IK(End) SWBD_IK(Esc) SWBD_IK(Enter)
	SWBD_IK(backspace) SWBD_IK(Del) SWBD_IK(space) SWBD_IK(tab) SWBD_IK(left)
	SWBD_IK(down) SWBD_IK(up) SWBD_IK(right) SWBD_IK(PrtSc) SWBD_IK(Menu)
}
#undef SWBD_IK


/** Initialize a kbd_key
 *  @param btn The Fl_Button which may be shared with other keys
 *  @param title The text that will be displayed on the button
 *  @param col The horizontal index for where to place the button
 *  @param width_cols How many horizontal units this button occupies
 *  @param total_cols How many horizontal units are on this row
 *  @param y The vertical origin of this row
 *  @param height How tall this row is
 *  @param scrwidth How many pixels wide the window is, for calculating columns
 */
Key* Keyboard::add(Fl_Button* b, const char* title, int col, int width_cols,
		int total_cols, int y, int height) {
	int x = (col * ui->width) / total_cols;
	int w = (ui->width * width_cols) / total_cols;
	Key* k = new Key(b, x, y, w, height); 
	k->button->label(title);
	k->button->box(ui->theme_up);
	//TODO add callback
	return k;
}

/** Macros for calling kbd_add_key with the right arguments
 *  Add Key, Add Key Title, Add Key Width, Add Key Width Title
 *  TODO: some of these are rarely used, remove them
 */
#define SWBD_AK(k) \
	row[index] = add(k, #k, col, 1, cols, y, height); \
	++index; ++col;
#define SWBD_AKT(k, t) \
	row[index] = add(k, t,  col, 1, cols, y, height); \
	++index; ++col;
#define SWBD_AKW(k, c) \
	row[index] = add(k, #k, col, c, cols, y, height); \
	++index; col += c;
#define SWBD_AKWT(k, c, t) \
	row[index] = add(k, t,  col, c, cols, y, height); \
	++index; col += c;

/** Assigns the correct details to each key
 *  TODO: reduce hackiness, simplify row incrementation
 */
void Keyboard::initKeys() {
	Key** row = row_0_0; // pointer to an array of pointers
	int index = 0;
	int col = 0;
	int cols = 12;
	int y = 0;
	int height = (ui->height - ui->tray_btn_height) * 0.2; // 5 rows
	SWBD_AK(Esc)
	SWBD_AKT(_1, "1!") SWBD_AKT(_2, "2@") SWBD_AKT(_3, "3#") SWBD_AKT(_4, "4$")
	SWBD_AKT(_5, "5%") SWBD_AKT(_6, "6^") SWBD_AKT(_7, "7&") SWBD_AKT(_8, "8*")
	SWBD_AKT(_9, "9(") SWBD_AKT(_0, "0)") SWBD_AKT(backspace, "@<-")
	row = row_0_1;
	index = 0;
	col = 0;
	cols = 11;
	y += height;
	SWBD_AK(Q) SWBD_AK(W) SWBD_AK(E) SWBD_AK(R) SWBD_AK(T) SWBD_AK(Y) SWBD_AK(U)
	SWBD_AK(I) SWBD_AK(O) SWBD_AK(P) SWBD_AKT(quote, "'\"")

	row = row_0_2;
	index = 0;
	col = 0;
	cols = 22;
	y += height;
	SWBD_AKT(semicolon, ";:") SWBD_AKW(A, 2) SWBD_AKW(S, 2) SWBD_AKW(D, 2)
	SWBD_AKW(F, 2) SWBD_AKW(G, 2) SWBD_AKW(H, 2) SWBD_AKW(J, 2) SWBD_AKW(K, 2)
	SWBD_AKW(L, 2) SWBD_AKWT(Enter, 3, "@returnarrow")

	row = row_0_3;
	index = 0;
	col = 0;
	cols = 11;
	y += height;
	SWBD_AKT(Shift, "^") SWBD_AK(Z) SWBD_AK(X) SWBD_AK(C) SWBD_AK(V) SWBD_AK(B)
	SWBD_AK(N) SWBD_AK(M) SWBD_AKT(comma, ",<") SWBD_AKT(period, ".>")
	SWBD_AKT(slash, "/?")

	row = row_0_4;
	index = 0;
	col = 0;
	cols = 11;
	y += height;
	SWBD_AK(Fn) SWBD_AK(Ctrl) SWBD_AK(Alt) SWBD_AKWT(space, 3, "@line")
	SWBD_AKT(tab, "@->|") SWBD_AKT(left, "@<") SWBD_AKT(down, "@2>")
	SWBD_AKT(up, "@8>") SWBD_AKT(right, "@>")
}
#undef SWBD_AK
#undef SWBD_AKT
#undef SWBD_AKW
#undef SWBD_AKWT

void Keyboard::show(void) {
	for (int i = 0; i < 12; ++i)
		row_0_0[i]->show();
	for (int i = 0; i < 11; ++i)
		row_0_1[i]->show();
	for (int i = 0; i < 11; ++i)
		row_0_2[i]->show();
	for (int i = 0; i < 11; ++i)
		row_0_3[i]->show();
	for (int i = 0; i < 9; ++i)
		row_0_4[i]->show();
	//stub
}
void Keyboard::hide(void) {
	for (int i = 0; i < 12; ++i)
		row_0_0[i]->hide();
	for (int i = 0; i < 11; ++i)
		row_0_1[i]->hide();
	for (int i = 0; i < 11; ++i)
		row_0_2[i]->hide();
	for (int i = 0; i < 11; ++i)
		row_0_3[i]->hide();
	for (int i = 0; i < 9; ++i)
		row_0_4[i]->hide();
	//stub
}

void Keyboard::callbackTray(Fl_Widget* w, void* keyboard) {
	(void)w;
	Keyboard* kbd = (Keyboard*)keyboard;
	((Tray*)kbd->ui->tray)->hide();
	kbd->show();
	kbd->ui->mode = UI::MODE::KBD;
}

Keyboard::Keyboard(UI* ui) {
	this->ui = ui;
	sim = new KeySim();
	initBtns();
	initKeys();
	traybtn = new Fl_Button(0, 0, 0, 0, "Keyboard");
	traybtn->hide();
	traybtn->box(ui->theme_up);
	traybtn->callback(Keyboard::callbackTray, this);
}

