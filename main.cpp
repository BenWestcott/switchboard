#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include "ui.hpp"

using namespace SWITCHBOARD;
int main(int argc, char** argv) {
	UI* ui = new UI(720, 350);
	ui->window->end();
	ui->window->show(argc, argv);
	return Fl::run();
}

