## Main
Switchboard is a system designed around modules.  
The main module, activated by default on startup, is the keyboard.

Main is responsible for initializing the tray and the keyboard. The tray is
responsible for initializing the other modules, as well as showing and hiding
all of the modules.

## Tray
Showing and hiding is done through a global state indicating both the current
and previous mode (active module). When the tray is expanded, the current
module is hidden and set as the previous module. When the tray is collapsed,
the previous module is shown and set as the current module.

Currently, the module is responsible for providing the button widget to the
tray when it is initialized. This is so that the callback function can be
sent implementation-specific context data remembered by the FLTK widget,
avoiding global variables.

Buttons to access other modules are hardcoded, and planned modules are stubbed
out to facilitate this. Currently the only fully functional module is AV.

## Keyboard
The keyboard is the main and default module, but also the most complex one.  
The main keyboard module contains the logic of constructing, configuring,
showing, and hiding the FLTK buttons. The keyboard sim files are part of the
keyboard module but logically separated as they are responsible for sending
keypress events. This file can be modified in the future if FLTK adds support
for alternative display protocols.

As there are a large number of FLTK widgets to be initialized and configured,
there are several macros to reduce repetitive code. These get more complex than
I'd like though, and at some point a clean algorithmic solution should be made.

## Other Modules
Each module has three exposed functions: init, show, and hide.

Teardown functions are planned for correctness, but not strictly necessary as
they are only destroyed once when the program shuts down, and the operating
system will automatically free the memory for us in that case.

Init is to be called exactly once. Not calling it will cause access violations,
and calling it multiple times will leak memory.

Show and hide are called exclusively by the tray. Show enables the appropriate
FLTK widgets, and may also recalculate sizes and positions. Hide hides them.
