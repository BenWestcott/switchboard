#ifndef SWITCHBOARD_UI_H
#define SWITCHBOARD_UI_H
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Widget.H>

namespace SWITCHBOARD {
using namespace SWITCHBOARD;
class UI {
	public:
	UI(int width, int height);

	enum class MODE {
		KBD, NOTIFS, WINS, AV, RADIOS, POWER
	} mode;
	unsigned int width;
	unsigned int height;
	unsigned int tray_btn_height;
	Fl_Boxtype theme_up, theme_dn;
	Fl_Window* window;
	// These cannot be class pointers as that creates a circular dependency
	void* tray; // Tray
	void* kbd; // Keyboard
	void* av; // AudioVideo
	void* pwr; // Power
	void* min; // Minimize
};
} //namespace

#endif

