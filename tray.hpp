#ifndef SWBD_TRAY_H
#define SWBD_TRAY_H
#include <FL/Fl.H>
#include <FL/Fl_Button.H>
#include "ui.hpp"
#include "modules/av.hpp"
#include "modules/kbd.hpp"
#include "modules/minimize.hpp"
//#include "modules/notifs.hpp"
#include "modules/power.hpp"
//#include "modules/radios.hpp"
//#include "modules/wins.hpp"

namespace SWITCHBOARD{
using namespace SWITCHBOARD;
class Tray {
	public:
	Tray(UI* ui);
	void hide(void); // used by tray button callbacks
	static void callback(Fl_Widget* w, void* tray);

	private:
	void show(void);
	UI* ui;
	int btnWidth, btnHeight;
	Fl_Button *toggle;

	friend void Minimize::callback(Fl_Widget* w, void* minimize);
	// Shortcuts to ui->[thing]->traybtn
	Fl_Button *minimize, *keyboard, *audioVideo, *power;
	// *windows, *notifications, *radios
};
} //namespace

#endif

