#include <stdio.h>
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Button.H>
#include "tray.hpp"
#include "ui.hpp"
#include "modules/minimize.hpp"


using namespace SWITCHBOARD;

Tray::Tray(UI* ui) {
	this->ui = ui;

	toggle = new Fl_Button(
		ui->width * 0.2, ui->height - ui->tray_btn_height,
		ui->width * 0.6, ui->tray_btn_height, "^");
	toggle->box(ui->theme_up);
	toggle->when(FL_WHEN_CHANGED);
	toggle->callback(Tray::callback, this);

	minimize = ((Minimize*)ui->min)->traybtn;
	minimize->resize(
		0,  ui->height - ui->tray_btn_height,
		ui->width * 0.2, ui->tray_btn_height);
	minimize->show();

	/*windows = swbd_module_wins_init(w);
	windows->resize(
		ui->width * 0.8, ui->height - ui->tray_btn_height,
		ui->width * 0.2, ui->tray_btn_height);
	windows->show();*/

	keyboard = ((Keyboard*)ui->kbd)->traybtn;
	//notifications = ((Notifs*)ui->notifs->traybtn;
	audioVideo = ((AudioVideo*)ui->av)->traybtn;
	//radios = ;
	power = ((Power*)ui->pwr)->traybtn;

	btnWidth  = ui->width / 3;
	btnHeight = (ui->height - ui->tray_btn_height) * 0.5;
	keyboard     ->resize(0*btnWidth, 0*btnHeight, btnWidth, btnHeight);
	//notifications->resize(1*btnWidth, 0*btnHeight, btnWidth, btnHeight);
	audioVideo   ->resize(0*btnWidth, 1*btnHeight, btnWidth, btnHeight);
	//radios       ->resize(1*btnWidth, 1*btnHeight, btnWidth, btnHeight);
	power        ->resize(2*btnWidth, 0*btnHeight, btnWidth, btnHeight);

}

void Tray::hide(void) {
	toggle->label("^");
	keyboard->hide();
	//notifications->hide();
	audioVideo->hide();
	//radios->hide();
	power->hide();
}

void Tray::show(void) {
	toggle->label("v");
	keyboard->show();
	//notifications->show();
	audioVideo->show();
	//radios->show();
	power->show();
}

void Tray::callback(Fl_Widget* w, void* tray) {
	Fl_Button* btn = (Fl_Button*)w;
	if (!btn->value()) // don't trigger on btn-up event
		return;
	Tray* t = (Tray*)tray;
	// toggle the open/closed state of the tray
	if (btn->label()[0] == 'v') {
		// tray is open; close and go back to the last module
		t->hide();
		switch(t->ui->mode) {
			case UI::MODE::KBD:
				((Keyboard*)t->ui->kbd)->show(); return;
			//case UI::MODE::NOTIFS:
			//	t->ui->notifs->show(); return;
			//case UI::MODE::WINS:
			//	t->ui->wins->show(); return;
			case UI::MODE::AV:
				((AudioVideo*)t->ui->av)->show(); return;
			//case UI::MODE::RADIOS:
			//	t->ui->radios->show(); return;
			case UI::MODE::POWER:
				((Power*)t->ui->pwr)->show(); return;
		}
	}
	// else: tray is closed; close the current module then open the tray
	switch(t->ui->mode) {
		case UI::MODE::KBD:
			((Keyboard*)t->ui->kbd)->hide(); break;
		//case UI::MODE::NOTIFS:
		//	t->ui->notifs->hide(); break;
		//case UI::MODE::WINS:
		//	t->ui->wins->hide(); break;
		case UI::MODE::AV:
			((AudioVideo*)t->ui->av)->hide(); break;
		//case UI::MODE::RADIOS:
		//	t->ui->radios->hide(); break;
		case UI::MODE::POWER:
			((Power*)t->ui->pwr)->hide(); break;
	}
	t->show();
}

