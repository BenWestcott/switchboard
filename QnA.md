### The keys on the keyboard don't work!
Yes, I can't figure out how to make the X11 keysym interface work. If you know how to
use the interface, help would be greatly appreciated.

### Why C++?
That's what FLTK was written in, and I don't want to have to deal with bindings.

### Why FLTK?
It's not as full-featured as Qt or GTK, but it's very lightweight and easy to use.
I also thought it would be fun to try out something new, and possibly underrepresented.

### Can you add animations?
Another limitation of FLTK is there is no built-in animation support. Implementing
animations myself would likely be a lot of effort and result in complex code. It would
also greatly reduce responsiveness on low-end devices, and the ability to enable or
disable animations would add even more complecity. Switchboard aims to be simple, fast,
hackable, and easy to understand.

### It doesn't work / does weird things on my device!
Switchboard was designed for use on the PinePhone in portrait mode. You will likely
need to change some constants and recompile for other devices.  
Some features, particularly backlight control, will also require file permission tweaks
in a boot-up script, as files on sysfs are not writable by default.

### The window doesn't resize!
Sizes and positions for widgets are calculated in init functions based on the constants
set in main. These could be instead calculated in a resize callback, but that is not
a priority, as it would not be helpful for the target use-case (smartphones and PDAs).

