#!/bin/sh
#todo: find where the list of c++ checkers are documented
#-enable-checker core.builtin.NoReturnFunctions \
#-enable-checker security.insecureAPI.rand \
#-enable-checker security.insecureAPI.strcpy \
scan-build -maxloop 12 \
-enable-checker core.AdjustedReturnValue \
-enable-checker core.AttributeNonNull \
-enable-checker core.CallAndMessage \
-enable-checker core.DivideZero \
-enable-checker core.NullDereference \
-enable-checker core.StackAddressEscape \
-enable-checker core.UndefinedBinaryOperatorResult \
-enable-checker core.VLASize \
-enable-checker core.builtin.BuiltinFunctions \
-enable-checker core.uninitialized.ArraySubscript \
-enable-checker core.uninitialized.Assign \
-enable-checker core.uninitialized.Branch \
-enable-checker core.uninitialized.CapturedBlockVariable \
-enable-checker core.uninitialized.UndefReturn \
-enable-checker cplusplus.InnerPointer \
-enable-checker cplusplus.Move \
-enable-checker cplusplus.NewDelete \
-enable-checker cplusplus.NewDeleteLeaks \
-enable-checker cplusplus.PlacementNew \
-enable-checker cplusplus.PureVirtualCall \
-enable-checker deadcode.DeadStores \
-enable-checker security.FloatLoopCounter \
-enable-checker security.insecureAPI.UncheckedReturn \
-enable-checker security.insecureAPI.getpw \
-enable-checker security.insecureAPI.gets \
-enable-checker security.insecureAPI.mkstemp \
-enable-checker security.insecureAPI.mktemp \
-enable-checker security.insecureAPI.vfork \
-enable-checker unix.API \
-enable-checker unix.Malloc \
-enable-checker unix.cstring.BadSizeArg \
-enable-checker unix.cstring.NullArg \
-o ./analyzer ./build.sh

